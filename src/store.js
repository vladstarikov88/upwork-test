import Vue from 'vue'
import Vuex from 'vuex'
import { genetareId } from '@/assets/helpers'
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    todoItems: []
  },
  mutations: {
    addTodoItem(state, payload) {
      state.todoItems.push(payload)
    },
    deleteTodoItem(state, payload) {
      const index = state.todoItems.findIndex(item => {
        return item.id === payload
      })

      state.todoItems.splice(index, 1)
    },
    checkTodoItem(state, payload) {
      const index = state.todoItems.findIndex(item => {
        return item.id === payload
      })
      const newItem = state.todoItems.find(item => {
        return item.id === payload
      })

      state.todoItems.splice(index, 1, {
        ...newItem,
        done: true
      })
    }
  },
  actions: {
    addTodoItem({commit}, title) {
      const id = genetareId();

      commit('addTodoItem', {
        id,
        title,
        done: false
      })
    },
    deleteTodoItem({commit}, id) {
      commit('deleteTodoItem', id)
    },
    checkTodoItem({commit}, id) {
      commit('checkTodoItem', id)
    }
  }
})
